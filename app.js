const express = require('express');
const path = require('path');
const bodyParser = require('body-parser'); // it parses incoming request body when you submit a form
const cors = require('cors'); //cors middleware it allows us to make a request to our api from different domain name
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

//To connec to mongodb
mongoose.connect(config.database);

// On connection
mongoose.connection.on('connected', () => {
  console.log('Connected to database '+config.database);
});

// On Error 
mongoose.connection.on('error', (error) => {
  console.log('Database error: '+error);
});

//initialize app variable with express
const app = express();

const users = require('./routes/users');

//const port = 3000;
const port = process.env.PORT || 8080;

//Cors Middleware
app.use(cors());

//Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

//Body Parser Middleware
app.use(bodyParser.json());

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);

//Index Route
app.get('/', (req, res) => {
  res.send('Invalid endpoint');
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

//Start Server
app.listen(port, () => {
  console.log('Server started on port '+port);

});
