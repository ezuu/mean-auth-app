const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

//Regiser
router.post('/register', (req, res, next) => {
  let newUser = new User({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password
  });
  User.addUser(newUser, (error, user) => {
    if (error) {
      res.json({success: false, message: 'Failed to register user'});
    } else {
      res.json({success: true, message: 'Successfully registered user'});
    }
  });
  //res.send('REGISTER');
});

//Authenticate
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.findOne({username: username}, (err, user) => {
    if (err) throw err;
    if(!user){
      res.json({success:false, message:'Authentication failed! User not found.'});
    } else if (user) {

      User.comparePassword(password, user.password, (err, isMatch) => {
        if (err) throw err;

        if (isMatch) {

          const token = jwt.sign(user.toJSON(), config.secret, {
            expiresIn: 604800 //1 week
          });

          res.json({
            success: true,
            token: 'JWT '+token,
            //token: token,
            user: {
              id: user._id,
              name: user.name,
              username: user.username,
              email: user.email
            }
          });

        } else {
          return res.json({success: false, msg: 'Wrong Password'});
        }


      });
    } 


  });

});

//Profile
router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  res.json({user: req.user});
  //res.send('Profile');
});


module.exports = router;
